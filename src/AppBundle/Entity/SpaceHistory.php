<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SpaceHistory
 *
 * @ORM\Table(name="space_history")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SpaceHistoryRepository")
 */
class SpaceHistory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="space_id", type="integer", nullable=true)
     */
    private $space;

    /**
     * @var int
     *
     * @ORM\Column(name="rent_id", type="integer")
     */
    private $rentId;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rentId
     *
     * @param integer $rentId
     *
     * @return SpaceHistory
     */
    public function setRentId($rentId)
    {
        $this->rentId = $rentId;

        return $this;
    }

    /**
     * Get rentId
     *
     * @return int
     */
    public function getRentId()
    {
        return $this->rentId;
    }

    /**
     * @return int
     */
    public function getSpace()
    {
        return $this->space;
    }

    /**
     * @param int $space
     * @return SpaceHistory
     */
    public function setSpace($space)
    {
        $this->space = $space;
        return $this;
    }



}

