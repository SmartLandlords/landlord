<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * ClientOffer
 *
 * @ORM\Table(name="client_offer")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClientOfferRepository")
 * @UniqueEntity(
 *     fields={"docNumber"},
 *     errorPath="docNumber",
 *     message="This document number is already used. Try another one!"
 * )
 */
class ClientOffer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="DocNumber", type="integer")
     */
    private $docNumber=0;

    /**
     * @var Client
     *
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="offer")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id", nullable=false)
     */

    private $client;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer", nullable=false)
     */
    private $price;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="date")
     */
    private $startDate;

    /**
     * @var Space
     *
     * @ORM\ManyToOne(targetEntity="Space", inversedBy="offer")
     * @ORM\JoinColumn(name="space_id", referencedColumnName="id", nullable=false)
     */
    private $space;

    /**
     * @var OfferItem[]
     * @ORM\OneToMany(targetEntity="OfferItem", mappedBy="offer")
     */
    private $offerItems;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="date")
     */
    private $endDate;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set docNumber
     *
     * @param integer $docNumber
     *
     * @return ClientOffer
     */
    public function setDocNumber($docNumber)
    {
        $this->docNumber = $docNumber;

        return $this;
    }

    /**
     * Get docNumber
     *
     * @return int
     */
    public function getDocNumber()
    {
        return $this->docNumber;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Client $client
     * @return ClientOffer
     */
    public function setClient($client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return ClientOffer
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return Space
     */
    public function getSpace()
    {
        return $this->space;
    }

    /**
     * @param Space $space
     * @return ClientOffer
     */
    public function setSpace($space)
    {
        $this->space = $space;
        return $this;
    }

    /**
     * @return OfferItem[]
     */
    public function getOfferItems()
    {
        return $this->offerItems;
    }

    /**
     * @param OfferItem[] $offerItems
     * @return ClientOffer
     */
    public function setOfferItems($offerItems)
    {
        $this->offerItems = $offerItems;
        return $this;
    }


    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return ClientOffer
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return ClientOffer
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    function __toString()
    {
        return  strval($this->price)." DocNb: ".strval($this->docNumber);
    }

}

