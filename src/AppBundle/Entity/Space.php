<?php

namespace AppBundle\Entity;


use AppBundle\Entity\Personnel\Cleaning;
use AppBundle\Entity\Personnel\Electrical;
use AppBundle\Entity\Personnel\Finance;
use AppBundle\Entity\Personnel\Inventory;
use AppBundle\Entity\Personnel\Plumbing;
use AppBundle\Entity\Personnel\Safety;
use AppBundle\Entity\Personnel\Sales;
use AppBundle\Entity\Personnel\Security;
use Doctrine\ORM\Mapping as ORM;

/**
 * Space
 *
 * @ORM\Table(name="space")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SpaceRepository")
 */
class Space
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name='';

    /**
     * @var SpaceHistory
     *
     * @ORM\ManyToOne(targetEntity="SpaceType", inversedBy="space")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $type;

    /**
     * @var float
     *
     * @ORM\Column(name="surface", type="float", nullable=true)
     */
    private $surface;

    /**
     * @var bool
     *
     * @ORM\Column(name="isCommercial", type="boolean")
     */
    private $isCommercial;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $admin;


    /**
     * @var Address
     *
     * @ORM\OneToOne(targetEntity="Address", mappedBy="space")
     */
    private $address;

    /**
     * @var GpsCoord[]
     *
     * @ORM\OneToMany(targetEntity="GpsCoord", mappedBy="space")
     */
    private $coords;

    /**
     * @var Utility[]
     *
     * @ORM\ManyToMany(targetEntity="Utility", inversedBy="space")
     * @ORM\JoinTable(name="utility_item")
     */
    private $utilities;

    /**
     * @var Safety[]
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Personnel\Safety", mappedBy="responsabilities")
     */
    private $safety;


    /**
     * @var Cleaning[]
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Personnel\Cleaning", mappedBy="responsabilities")
     */
    private $cleaning;

    /**
     * @var Electrical[]
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Personnel\Electrical", mappedBy="responsabilities")
     */
    private $electrical;

    /**
     * @var Finance[]
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Personnel\Finance", mappedBy="responsabilities")
     */
    private $finance;

    /**
     * @var Inventory[]
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Personnel\Inventory", mappedBy="responsabilities")
     */
    private $inventory;

    /**
     * @var Plumbing[]
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Personnel\Plumbing", mappedBy="responsabilities")
     */
    private $plumbing;



    /**
     * @var Sales[]
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Personnel\Sales", mappedBy="responsabilities")
     */
    private $sales;

    /**
     * @var Security
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Personnel\Security", mappedBy="responsabilities")
     */
    private $security;

    /**
     * @return Security
     */
    public function getSecurity()
    {
        return $this->security;
    }

    /**
     * @param Security $security
     * @return Space
     */
    public function setSecurity($security)
    {
        $this->security = $security;
        return $this;
    }










    /**
     * @return Sales[]
     */
    public function getSales()
    {
        return $this->sales;
    }

    /**
     * @param Sales[] $sales
     * @return Space
     */
    public function setSales($sales)
    {
        $this->sales = $sales;
        return $this;
    }




    /**
     * @return Plumbing[]
     */
    public function getPlumbing()
    {
        return $this->plumbing;
    }

    /**
     * @param Plumbing[] $plumbing
     */
    public function setPlumbing($plumbing)
    {
        $this->plumbing = $plumbing;
    }




    /**
     * @return Inventory[]
     */
    public function getInventory()
    {
        return $this->inventory;
    }

    /**
     * @param Inventory[] $inventory
     * @return Space
     */
    public function setInventory($inventory)
    {
        $this->inventory = $inventory;
        return $this;
    }




    /**
     * @return Finance[]
     */
    public function getFinance()
    {
        return $this->finance;
    }

    /**
     * @param Finance[] $finance
     * @return Space
     */
    public function setFinance($finance)
    {
        $this->finance = $finance;
        return $this;
    }



    /**
     * @return Electrical[]
     */
    public function getElectrical()
    {
        return $this->electrical;
    }

    /**
     * @param Electrical[] $electrical
     * @return Space
     */
    public function setElectrical($electrical)
    {
        $this->electrical = $electrical;
        return $this;
    }



    /**
     * @return Cleaning[]
     */
    public function getCleaning()
    {
        return $this->cleaning;
    }

    /**
     * @param Cleaning[] $cleaning
     * @return Space
     */
    public function setCleaning($cleaning)
    {
        $this->cleaning = $cleaning;
        return $this;
    }



    /**
     * @return Safety[]
     */
    public function getSafety()
    {
        return $this->safety;
    }

    /**
     * @param Safety[] $safety
     * @return Space
     */
    public function setSafety($safety)
    {
        $this->safety = $safety;
        return $this;
    }





    /**
     * @var Space
     *
     * @ORM\ManyToOne(targetEntity="Space", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $parent;

    /**
     * @var Space[]
     *
     * @ORM\OneToMany(targetEntity="Space", mappedBy="parent", cascade={"persist"})
     */
    private $children;

    /**
     * @var Image[]
     *
     * @ORM\OneToMany(targetEntity="Image", mappedBy="space")
     */
    private $image;

    /**
     * @var Client
     *
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="space")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @var RentContract
     *
     * @ORM\ManyToOne(targetEntity="RentContract", inversedBy="space")
     * @ORM\JoinColumn(name="rent_id", referencedColumnName="id")
     */
    private $rent;

    /**
     * @var ClientOffer
     *
     * @ORM\ManyToOne(targetEntity="ClientOffer", inversedBy="space")
     * @ORM\JoinColumn(name="offer_id", referencedColumnName="id")
     */
    private $offer;

    /**
     * @var ContractItem
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ContractItem", inversedBy="space")
     * @ORM\JoinColumn(name="ContractItem_id", referencedColumnName="id")
     */
    private $contractItems;

    /**
     * @return ContractItem
     */
    public function getContractItems()
    {
        return $this->contractItems;
    }

    /**
     * @param ContractItem $contractItems
     * @return Space
     */
    public function setContractItems($contractItems)
    {
        $this->contractItems = $contractItems;
        return $this;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Space
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return SpaceHistory
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param SpaceHistory $type
     * @return Space
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }


    /**
     * Set surface
     *
     * @param float $surface
     *
     * @return Space
     */
    public function setSurface($surface)
    {
        $this->surface = $surface;

        return $this;
    }

    /**
     * Get surface
     *
     * @return float
     */
    public function getSurface()
    {
        return $this->surface;
    }

    /**
     * Set isCommercial
     *
     * @param boolean $isCommercial
     *
     * @return Space
     */
    public function setIsCommercial($isCommercial)
    {
        $this->isCommercial = $isCommercial;

        return $this;
    }

    /**
     * Get isCommercial
     *
     * @return bool
     */
    public function getIsCommercial()
    {
        return $this->isCommercial;
    }

    /**
     * @return User
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * @param User $admin
     * @return Space
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;
        return $this;
    }
        
    /**
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param Address $address
     * @return Space
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return GpsCoord[]
     */
    public function getCoords()
    {
        return $this->coords;
    }

    /**
     * @param GpsCoord[] $coords
     * @return Space
     */
    public function setCoords($coords)
    {
        $this->coords = $coords;
        return $this;
    }

    /**
     * @return Utility[]
     */
    public function getUtilities()
    {
        return $this->utilities;
    }

    /**
     * @param Utility[] $utilities
     * @return Space
     */
    public function setUtilities($utilities)
    {
        $this->utilities = $utilities;
        return $this;
    }

    /**
     * @return Space
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Space $parent
     * @return Space
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return Space[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param Space[] $children
     * @return Space
     */
    public function setChildren($children)
    {
        $this->children = $children;
        return $this;
    }

    /**
     * @return Image[]
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param Image[] $image
     * @return Space
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return Client
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param Client $owner
     * @return Space
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @return RentContract
     */
    public function getRent()
    {
        return $this->rent;
    }

    /**
     * @param RentContract $rent
     * @return Space
     */
    public function setRent($rent)
    {
        $this->rent = $rent;
        return $this;
    }

    /**
     * @return ClientOffer
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * @param ClientOffer $offer
     * @return Space
     */
    public function setOffer($offer)
    {
        $this->offer = $offer;
        return $this;
    }

    function __toString()
    {
        if ($this->parent == null)
          {$sep = " "; }
          else {$sep = " : ";}

        return $this->name.$sep.$this->parent;
    }



}

