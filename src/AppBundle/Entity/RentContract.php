<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RentContract
 *
 * @ORM\Table(name="rent_contract")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RentContractRepository")
 */
class RentContract
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="DocNumber", type="integer", unique=true)
     */
    private $docNumber=0;

    /**
     * @var Client
     *
     * @ORM\OneToOne(targetEntity="Client", inversedBy="rentOwner")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @var Client
     *
     * @ORM\OneToOne(targetEntity="Client", inversedBy="rentClient")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer", nullable=true, unique=true)
     */
    private $price;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="date")
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="date")
     */
    private $endDate;

    /**
     * @var string
     *
     * @ORM\Column(name="clauses", type="string", length=1000, nullable=true)
     */
    private $clauses;

    /**
     * @var Invoice
     *
     * @ORM\ManyToOne(targetEntity="Invoice", inversedBy="rent")
     * @ORM\JoinColumn(name="invoice_id", referencedColumnName="id")
     */
    private $invoice;

    /**
     * @var Space[]
     *
     * @ORM\OneToMany(targetEntity="Space", mappedBy="rent")
     */
    private $space;

    /**
     * @var ContractItem[]
     *
     * @ORM\OneToMany(targetEntity="ContractItem", mappedBy="rent")
     */
    private $items;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set docNumber
     *
     * @param integer $docNumber
     *
     * @return RentContract
     */
    public function setDocNumber($docNumber)
    {
        $this->docNumber = $docNumber;

        return $this;
    }

    /**
     * Get docNumber
     *
     * @return int
     */
    public function getDocNumber()
    {
        return $this->docNumber;
    }

    /**
     * @return Client
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param Client $owner
     * @return RentContract
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Client $client
     * @return RentContract
     */
    public function setClient($client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return RentContract
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return RentContract
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param string $endDate
     *
     * @return RentContract
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return string
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set clauses
     *
     * @param string $clauses
     *
     * @return RentContract
     */
    public function setClauses($clauses)
    {
        $this->clauses = $clauses;

        return $this;
    }

    /**
     * Get clauses
     *
     * @return string
     */
    public function getClauses()
    {
        return $this->clauses;
    }

    /**
     * @return Invoice
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @param Invoice $invoice
     * @return RentContract
     */
    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;
        return $this;
    }


    /**
     * @return Space[]
     */
    public function getSpace()
    {
        return $this->space;
    }

    /**
     * @param Space[] $space
     * @return RentContract
     */
    public function setSpace($space)
    {
        $this->space = $space;
        return $this;
    }

    /**
     * @return ContractItem[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param ContractItem[] $items
     * @return RentContract
     */
    public function setItems($items)
    {
        $this->items = $items;
        return $this;
    }

    function __toString()
    {
        return strval($this->docNumber);
    }


}

