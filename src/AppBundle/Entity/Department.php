<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Personnel\Cleaning;
use AppBundle\Entity\Personnel\Electrical;
use AppBundle\Entity\Personnel\Finance;
use AppBundle\Entity\Personnel\Inventory;
use AppBundle\Entity\Personnel\Plumbing;
use AppBundle\Entity\Personnel\Safety;
use AppBundle\Entity\Personnel\Sales;
use AppBundle\Entity\Personnel\Security;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Department
 *
 * @ORM\Table(name="department")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DepartmentRepository")
 * @UniqueEntity(
 *     fields={"name"},
 *     errorPath="name",
 *     message="This name is already in use."
 * )
 */
class Department
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name='';


    /**
     * @var Contact[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Contact", mappedBy="department")
     */
    private $contacts;

    /**
     * @var Safety
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Personnel\Safety", inversedBy="department")
     * @ORM\JoinColumn(name="safety_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $safety;


    /**
     * @var Cleaning
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Personnel\Cleaning", inversedBy="department")
     * @ORM\JoinColumn(name="cleaning_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $cleaning;


    /**
     * @var Electrical
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Personnel\Electrical", inversedBy="department")
     * @ORM\JoinColumn(name="electrical_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $electrical;

    /**
     * @var Finance
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Personnel\Finance", inversedBy="department")
     * @ORM\JoinColumn(name="finance_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $finance;

    /**
     * @var Inventory
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Personnel\Inventory", inversedBy="department")
     * @ORM\JoinColumn(name="inventory_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $inventory;


    /**
     * @var Plumbing
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Personnel\Plumbing", inversedBy="department")
     * @ORM\JoinColumn(name="plumbing_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $plumbing;



    /**
     * @var Sales
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Personnel\Sales", inversedBy="department")
     * @ORM\JoinColumn(name="sales_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $sales;



    /**
     * @var Security
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Personnel\Security", inversedBy="department")
     * @ORM\JoinColumn(name="security_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $security;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Department
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Department
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Contact[]
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * @param Contact[] $contacts
     * @return Department
     */
    public function setContacts($contacts)
    {
        $this->contacts = $contacts;
        return $this;
    }

    /**
     * @return Safety
     */
    public function getSafety()
    {
        return $this->safety;
    }

    /**
     * @param Safety $safety
     * @return Department
     */
    public function setSafety($safety)
    {
        $this->safety = $safety;
        return $this;
    }

    /**
     * @return Cleaning
     */
    public function getCleaning()
    {
        return $this->cleaning;
    }

    /**
     * @param Cleaning $cleaning
     * @return Department
     */
    public function setCleaning($cleaning)
    {
        $this->cleaning = $cleaning;
        return $this;
    }

    /**
     * @return Electrical
     */
    public function getElectrical()
    {
        return $this->electrical;
    }

    /**
     * @param Electrical $electrical
     * @return Department
     */
    public function setElectrical($electrical)
    {
        $this->electrical = $electrical;
        return $this;
    }

    /**
     * @return Finance
     */
    public function getFinance()
    {
        return $this->finance;
    }

    /**
     * @param Finance $finance
     * @return Department
     */
    public function setFinance($finance)
    {
        $this->finance = $finance;
        return $this;
    }

    /**
     * @return Inventory
     */
    public function getInventory()
    {
        return $this->inventory;
    }

    /**
     * @param Inventory $inventory
     * @return Department
     */
    public function setInventory($inventory)
    {
        $this->inventory = $inventory;
        return $this;
    }

    /**
     * @return Plumbing
     */
    public function getPlumbing()
    {
        return $this->plumbing;
    }

    /**
     * @param Plumbing $plumbing
     * @return Department
     */
    public function setPlumbing($plumbing)
    {
        $this->plumbing = $plumbing;
        return $this;
    }

    /**
     * @return Sales
     */
    public function getSales()
    {
        return $this->sales;
    }

    /**
     * @param Sales $sales
     * @return Department
     */
    public function setSales($sales)
    {
        $this->sales = $sales;
        return $this;
    }

    /**
     * @return Security
     */
    public function getSecurity()
    {
        return $this->security;
    }

    /**
     * @param Security $security
     * @return Department
     */
    public function setSecurity($security)
    {
        $this->security = $security;
        return $this;
    }
    function __toString()
    {
        return $this->name;
    }



}


