<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OfferItem
 *
 * @ORM\Table(name="offer_item")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OfferItemRepository")
 */
class OfferItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="space_id", type="integer", nullable=true, unique=true)
     */
    private $space;

    /**
     * @var ClientOffer
     *
     * @ORM\ManyToOne(targetEntity="ClientOffer", inversedBy="offerItems")
     * @ORM\JoinColumn(name="offer_id", referencedColumnName="id")
     */
    private $offer;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getSpace()
    {
        return $this->space;
    }

    /**
     * @param int $space
     * @return OfferItem
     */
    public function setSpace($space)
    {
        $this->space = $space;
        return $this;
    }



    /**
     * @return ClientOffer
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * @param ClientOffer $offer
     * @return OfferItem
     */
    public function setOffer($offer)
    {
        $this->offer = $offer;
        return $this;
    }


}

