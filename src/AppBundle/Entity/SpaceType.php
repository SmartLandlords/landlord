<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SpaceType
 *
 * @ORM\Table(name="space_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SpaceTypeRepository")
 */
class SpaceType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name='';

    /**
     * @var Space[]
     *
     *  @ORM\OneToMany(targetEntity="AppBundle\Entity\Space", mappedBy="type")
     */
    private $space;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SpaceType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Space[]
     */
    public function getSpace()
    {
        return $this->space;
    }

    /**
     * @param Space[] $space
     * @return SpaceType
     */
    public function setSpace($space)
    {
        $this->space = $space;
        return $this;
    }

    function __toString()
    {
        return $this->name;
    }


}

