<?php

namespace AppBundle\Entity\Personnel;

use AppBundle\Entity\Department;
use AppBundle\Entity\Space;
use Doctrine\ORM\Mapping as ORM;

/**
 * Electrical
 *
 * @ORM\Table(name="personnel_electrical")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Personnel\ElectricalRepository")
 */
class Electrical
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var Space[]
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Space", inversedBy="electrical")
     * @ORM\JoinTable(name="electrical_id")
     */
    private $responsabilities;
    /**
     * @var string
     *
     * @ORM\Column(name="deadline", type="string", length=255)
     */
    private $deadline;

    /**
     * @var Department
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Department", mappedBy="electrical")
     */
    private $department;

    /**
     * @return Department
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param Department $department
     * @return Electrical
     */
    public function setDepartment($department)
    {
        $this->department = $department;
        return $this;
    }






    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Electrical
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Space[]
     */
    public function getResponsabilities()
    {
        return $this->responsabilities;
    }

    /**
     * @param Space[] $responsabilities
     * @return Electrical
     */
    public function setResponsabilities($responsabilities)
    {
        $this->responsabilities = $responsabilities;
        return $this;
    }



    /**
     * @return string
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * @param string $deadline
     * @return Electrical
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;
        return $this;
    }


}

