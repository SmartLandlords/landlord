<?php

namespace AppBundle\Entity\Personnel;

use AppBundle\Entity\Department;
use AppBundle\Entity\Space;
use Doctrine\ORM\Mapping as ORM;

/**
 * Sales
 *
 * @ORM\Table(name="personel_sales")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Personnel\SalesRepository")
 */
class Sales
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var Space[]
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Space", inversedBy="sales")
     * @ORM\JoinTable(name="sales_id")
     */
    private $responsabilities;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deadline", type="datetime")
     */
    private $deadline;

    /**
     * @var Department
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Department", mappedBy="safety")
     */
    private $department;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Sales
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set responsabilities
     *
     * @param string $responsabilities
     *
     * @return Sales
     */
    public function setResponsabilities($responsabilities)
    {
        $this->responsabilities = $responsabilities;

        return $this;
    }

    /**
     * Get responsabilities
     *
     * @return string
     */
    public function getResponsabilities()
    {
        return $this->responsabilities;
    }

    /**
     * @return \DateTime
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * @param \DateTime $deadline
     * @return Sales
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;
        return $this;
    }

    /**
     * @return Department
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param Department $department
     * @return Sales
     */
    public function setDepartment($department)
    {
        $this->department = $department;
        return $this;
    }







}

