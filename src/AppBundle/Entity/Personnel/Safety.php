<?php

namespace AppBundle\Entity\Personnel;

use AppBundle\Entity\Department;
use AppBundle\Entity\Space;
use Doctrine\ORM\Mapping as ORM;

/**
 * Safety
 *
 * @ORM\Table(name="personnel_safety")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SafetyRepository")
 */
class Safety
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var Space[]
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Space", inversedBy="safety")
     * @ORM\JoinTable(name="safety_id")
     */
    private $responsabilities;

    /**
     * @var string
     *
     * @ORM\Column(name="resource", type="string", length=255)
     */
    private $resource;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deadline", type="datetime")
     */
    private $deadline;

    /**
     * @var Department
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Department", mappedBy="safety")
     */
    private $department;

    /**
     * @return Department
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param Department $department
     * @return Safety
     */
    public function setDepartment($department)
    {
        $this->department = $department;
        return $this;
    }









    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Safety
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Space[]
     */
    public function getResponsabilities()
    {
        return $this->responsabilities;
    }

    /**
     * @param Space[] $responsabilities
     * @return Safety
     */
    public function setResponsabilities($responsabilities)
    {
        $this->responsabilities = $responsabilities;
        return $this;
    }





    /**
     * Set resource
     *
     * @param string $resource
     *
     * @return Safety
     */
    public function setResource($resource)
    {
        $this->resource = $resource;

        return $this;
    }

    /**
     * Get resource
     *
     * @return string
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * @return \DateTime
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * @param \DateTime $deadline
     * @return Safety
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;
        return $this;
    }

    function __toString()
    {
        return $this->name;
    }


}

