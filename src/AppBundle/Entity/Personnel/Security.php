<?php

namespace AppBundle\Entity\Personnel;

use AppBundle\Entity\Department;
use AppBundle\Entity\Space;
use Doctrine\ORM\Mapping as ORM;

/**
 * Security
 *
 * @ORM\Table(name="personnel_security")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SecurityRepository")
 */
class Security
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var Space[]
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Space", inversedBy="security")
     * @ORM\JoinTable(name="security_id")
     */
    private $responsabilities;

    /**
     * @var string
     *
     * @ORM\Column(name="deadline", type="string", length=255)
     */
    private $deadline;

    /**
     * @var Department
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Department", mappedBy="security")
     */
    private $department;

    /**
     * @return Department
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param Department $department
     * @return Security
     */
    public function setDepartment($department)
    {
        $this->department = $department;
        return $this;
    }





    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Security
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Space[]
     */
    public function getResponsabilities()
    {
        return $this->responsabilities;
    }

    /**
     * @param Space[] $responsabilities
     * @return Security
     */
    public function setResponsabilities($responsabilities)
    {
        $this->responsabilities = $responsabilities;
        return $this;
    }





    /**
     * @return string
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * @param string $deadline
     * @return Security
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;
        return $this;
    }


}

