<?php

namespace AppBundle\Entity\Personnel;

use AppBundle\Entity\Department;
use AppBundle\Entity\Space;
use Doctrine\ORM\Mapping as ORM;

/**
 * Finance
 *
 * @ORM\Table(name="personnel_finance")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Personnel\FinanceRepository")
 */
class Finance
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var Space[]
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Space", inversedBy="finance")
     * @ORM\JoinTable(name="finance_id")
     */
    private $responsabilities;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deadline", type="datetime")
     */
    private $deadline;


    /**
     * @var Department
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Department", mappedBy="finance")
     */
    private $department;

    /**
     * @return Department
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param Department $department
     * @return Finance
     */
    public function setDepartment($department)
    {
        $this->department = $department;
        return $this;
    }








    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Finance
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Space[]
     */
    public function getResponsabilities()
    {
        return $this->responsabilities;
    }

    /**
     * @param Space[] $responsabilities
     * @return Finance
     */
    public function setResponsabilities($responsabilities)
    {
        $this->responsabilities = $responsabilities;
        return $this;
    }





    /**
     * @return \DateTime
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * @param \DateTime $deadline
     * @return Finance
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;
        return $this;
    }



}

