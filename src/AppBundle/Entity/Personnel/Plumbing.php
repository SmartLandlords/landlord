<?php

namespace AppBundle\Entity\Personnel;

use AppBundle\Entity\Department;
use AppBundle\Entity\Space;
use Doctrine\ORM\Mapping as ORM;

/**
 * Plumbing
 *
 * @ORM\Table(name="personnel_plumbing")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Personnel\PlumbingRepository")
 */
class Plumbing
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var Space[]
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Space", inversedBy="plumbing")
     * @ORM\JoinTable(name="plumbing_id")
     */
    private $responsabilities;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deadline", type="datetime")
     */
    private $deadline;


    /**
     * @var Department
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Department", mappedBy="plumbing")
     */
    private $department;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Plumbing
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set responsabilities
     *
     * @param string $responsabilities
     *
     * @return Plumbing
     */
    public function setResponsabilities($responsabilities)
    {
        $this->responsabilities = $responsabilities;

        return $this;
    }

    /**
     * Get responsabilities
     *
     * @return string
     */
    public function getResponsabilities()
    {
        return $this->responsabilities;
    }

    /**
     * @return \DateTime
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * @param \DateTime $deadline
     * @return Plumbing
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;
        return $this;
    }

    /**
     * @return Department
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param Department $department
     * @return Plumbing
     */
    public function setDepartment($department)
    {
        $this->department = $department;
        return $this;
    }








}

