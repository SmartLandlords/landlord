<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContractItem
 *
 * @ORM\Table(name="contract_item")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContractItemRepository")
 */
class ContractItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var RentContract
     *
     * @ORM\ManyToOne(targetEntity="RentContract", inversedBy="items")
     * @ORM\JoinColumn(name="rent_id", referencedColumnName="id")
     */
    private $rent;

    /**
     * @var Space[]
     *
     * @ORM\OneToMany(targetEntity="Space", mappedBy="contractItems")
     */
    private $space;

    /**
     * @return Space[]
     */
    public function getSpace()
    {
        return $this->space;
    }

    /**
     * @param Space[] $space
     * @return ContractItem
     */
    public function setSpace($space)
    {
        $this->space = $space;
        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return RentContract
     */
    public function getRent()
    {
        return $this->rent;
    }

    /**
     * @param RentContract $rent
     * @return ContractItem
     */
    public function setRent($rent)
    {
        $this->rent = $rent;
        return $this;
    }




}

