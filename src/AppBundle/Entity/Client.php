<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Client
 *
 * @ORM\Table(name="client")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClientRepository")
 */
class Client
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name='';

    /**
     * @var bool
     *
     * @ORM\Column(name="owner", type="boolean")
     */
    private $isOwner = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="company", type="boolean")
     */
    private $isCompany = true;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $admin;

    /**
     * @var Space[]
     *
     * @ORM\OneToMany(targetEntity="Space", mappedBy="owner")
     */
    private $space;


    /**
     * @var Contact[]
     *
     *  @ORM\OneToMany(targetEntity="AppBundle\Entity\Contact", mappedBy="client")
     */
     private $contacts;

    /**
     * @var ClientOffer[]
     *
     * @ORM\OneToMany(targetEntity="ClientOffer", mappedBy="client")
     */
     private $offer;

    /**
     * @var RentContract
     *
     * @ORM\OneToOne(targetEntity="RentContract", mappedBy="owner")
     */
     private $rentOwner;

    /**
     * @var RentContract
     *
     * @ORM\OneToOne(targetEntity="RentContract", mappedBy="client")
     */
     private $rentClient;

    /**
     * @var Invoice
     *
     * @ORM\OneToOne(targetEntity="Invoice", mappedBy="owner")
     */
     private $invoiceOwner;

    /**
     * @var Invoice
     *
     * @ORM\OneToOne(targetEntity="Invoice", mappedBy="client")
     */
     private $invoiceClient;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Client
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Client
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return bool
     */
    public function isOwner()
    {
        return $this->isOwner;
    }

    /**
     * @param bool $isOwner
     * @return Client
     */
    public function setIsOwner($isOwner)
    {
        $this->isOwner = $isOwner;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCompany()
    {
        return $this->isCompany;
    }

    /**
     * @param bool $isCompany
     * @return Client
     */
    public function setIsCompany($isCompany)
    {
        $this->isCompany = $isCompany;
        return $this;
    }

    /**
     * @return User
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * @param User $admin
     * @return Client
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;
        return $this;
    }

    /**
     * @return Space[]
     */
    public function getSpace()
    {
        return $this->space;
    }

    /**
     * @param Space[] $space
     * @return Client
     */
    public function setSpace($space)
    {
        $this->space = $space;
        return $this;
    }

    /**
     * @return Contact[]
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * @param Contact[] $contacts
     * @return Client
     */
    public function setContacts($contacts)
    {
        $this->contacts = $contacts;
        return $this;
    }

    /**
     * @return ClientOffer[]
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * @param ClientOffer[] $offer
     * @return Client
     */
    public function setOffer($offer)
    {
        $this->offer = $offer;
        return $this;
    }

    /**
     * @return RentContract
     */
    public function getRentOwner()
    {
        return $this->rentOwner;
    }

    /**
     * @param RentContract $rentOwner
     * @return Client
     */
    public function setRentOwner($rentOwner)
    {
        $this->rentOwner = $rentOwner;
        return $this;
    }

    /**
     * @return RentContract
     */
    public function getRentClient()
    {
        return $this->rentClient;
    }

    /**
     * @param RentContract $rentClient
     * @return Client
     */
    public function setRentClient($rentClient)
    {
        $this->rentClient = $rentClient;
        return $this;
    }

    /**
     * @return Invoice
     */
    public function getInvoiceOwner()
    {
        return $this->invoiceOwner;
    }

    /**
     * @param Invoice $invoiceOwner
     * @return Client
     */
    public function setInvoiceOwner($invoiceOwner)
    {
        $this->invoiceOwner = $invoiceOwner;
        return $this;
    }

    /**
     * @return Invoice
     */
    public function getInvoiceClient()
    {
        return $this->invoiceClient;
    }

    /**
     * @param Invoice $invoiceClient
     * @return Client
     */
    public function setInvoiceClient($invoiceClient)
    {
        $this->invoiceClient = $invoiceClient;
        return $this;
    }

    function __toString()
    {
        return $this->name;
    }




}

