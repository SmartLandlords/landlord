<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GpsCoord
 *
 * @ORM\Table(name="gps_coord")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GpsCoordRepository")
 */
class GpsCoord
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="lat", type="string", length=255)
     */
    private $lat='';

    /**
     * @var string
     *
     * @ORM\Column(name="longit", type="string", length=255)
     */
    private $longit='';

    /**
     * @var Space
     *
     * @ORM\ManyToOne(targetEntity="Space", inversedBy="coords")
     * @ORM\JoinColumn(name="space_id", referencedColumnName="id")
     */
    private $space;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lat
     *
     * @param string $lat
     *
     * @return GpsCoord
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return string
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set longit
     *
     * @param string $longit
     *
     * @return GpsCoord
     */
    public function setLongit($longit)
    {
        $this->longit = $longit;

        return $this;
    }

    /**
     * Get longit
     *
     * @return string
     */
    public function getLongit()
    {
        return $this->longit;
    }

    /**
     * @return Space
     */
    public function getSpace()
    {
        return $this->space;
    }

    /**
     * @param Space $space
     * @return GpsCoord
     */
    public function setSpace($space)
    {
        $this->space = $space;
        return $this;
    }

    function __toString()
    {
        $coord = array($this->lat,$this->longit);
        return implode("  ", $coord);
    }


}

