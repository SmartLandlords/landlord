<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Address
 *
 * @ORM\Table(name="address")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AddressRepository")
 */
class Address
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255)
     */
    private $city='';

    /**
     * @var string
     *
     * @ORM\Column(name="county", type="string", length=255)
     */
    private $county='';

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255)
     */
    private $street='';

    /**
     * @var string
     *
     * @ORM\Column(name="area", type="string", length=255)
     */
    private $area='';

    /**
     * @var Space
     *
     * @ORM\OneToOne(targetEntity="Space", inversedBy="address")
     * @ORM\JoinColumn(name="space_id", referencedColumnName="id")
     */
    private $space;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Address
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set county
     *
     * @param string $county
     *
     * @return Address
     */
    public function setCounty($county)
    {
        $this->county = $county;

        return $this;
    }

    /**
     * Get county
     *
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return Address
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set area
     *
     * @param string $area
     *
     * @return Address
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return string
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @return Space
     */
    public function getSpace()
    {
        return $this->space;
    }

    /**
     * @param Space $space
     * @return Address
     */
    public function setSpace($space)
    {
        $this->space = $space;
        return $this;
    }

    function __toString()
    {
        $adr = [$this->city, $this->county, $this->street, $this->area];
        return implode(", ", $adr);
    }


}

