<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ClientOfferAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('docNumber')
            ->add('client')
            ->add('price')
            ->add('space')
            ->add('startDate')
            ->add('endDate')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('docNumber')
            ->add('client')
            ->add('price')
            ->add('space')
            ->add('startDate')
            ->add('endDate')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('docNumber')
            ->add('client')
            ->add('price')
            ->add('space')
            ->add('startDate')
            ->add('endDate')
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('docNumber')
            ->add('client')
            ->add('price')
            ->add('space')
            ->add('startDate')
            ->add('endDate')
        ;
    }
}
