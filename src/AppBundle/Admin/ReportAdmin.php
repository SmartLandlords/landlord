<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ReportAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('docNumber')
            ->add('ownerId')
            ->add('clientId')
            ->add('date')
            ->add('description')
            ->add('rentId')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('docNumber')
            ->add('ownerId')
            ->add('clientId')
            ->add('date')
            ->add('description')
            ->add('rentId')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id')
            ->add('docNumber')
            ->add('ownerId')
            ->add('clientId')
            ->add('date')
            ->add('description')
            ->add('rentId')
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('docNumber')
            ->add('ownerId')
            ->add('clientId')
            ->add('date')
            ->add('description')
            ->add('rentId')
        ;
    }
}
