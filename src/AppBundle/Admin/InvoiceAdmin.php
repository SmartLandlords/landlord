<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class InvoiceAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('date')
            ->add('invoiceNumber')
            ->add('owner')
            ->add('client')
            ->add('price')
            ->add('vat')
            ->add('rent')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('date')
            ->add('invoiceNumber')
            ->add('owner')
            ->add('client')
            ->add('price')
            ->add('vat')
            ->add('rent')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('date')
            ->add('invoiceNumber')
            ->add('owner')
            ->add('client')
            ->add('price')
            ->add('vat')
            ->add('rent')
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('date')
            ->add('invoiceNumber')
            ->add('owner')
            ->add('client')
            ->add('price')
            ->add('vat')
            ->add('rent')
        ;
    }
}
